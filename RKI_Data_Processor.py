import requests
import json
import pandas as pd
from glob import glob
import os
from datetime import date, timedelta

today = str(date.today())  # ISO format, i.e. YYYY-MM-DD
yesterday = str(date.today() - timedelta(days=1))

if not os.path.isdir('Raw'):
    os.mkdir('Raw')
if not os.path.isdir('Data/'):
    os.mkdir('Data')

URL = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0/query?where=1%3D1&outFields=Bundesland,Landkreis,AnzahlFall,AnzahlTodesfall,Meldedatum,Datenstand,NeuerFall,NeuerTodesfall&outSR=4326&f=json"

loop = True
file_index = 0

if os.path.isdir('Raw/' + today):     # Clear up the directory before retrieving a new batch; not absolutely needed
    for file in glob('./Raw/' + today + '/*.json'):
        os.remove(file)
else:  # Create the directory for the current date; IS absolutely needed <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< !!!
    os.mkdir('Raw/' + today)

while loop:
    data = requests.get(URL, params={"resultRecordCount": 2000,
                                     "resultOffset": file_index * 2000,
                                     "supportsPagination": "true"}).json()
    with open("./Raw/" + today + "/RKI_COVID19_Data_" + str(file_index) + ".json", "w", encoding='utf8') as output:
        json.dump(data, output, ensure_ascii=False, indent=4)

    if "exceededTransferLimit" in data and data["exceededTransferLimit"]:
        file_index += 1
    else:
        loop = False

data_df = pd.DataFrame()
for file in glob("./Raw/" + today + "/*.json"):
    data = json.load(open(file,  "r", encoding='utf8'))
    data = [dict(x)['attributes'] for x in data['features']]
    data_df = data_df.append(pd.DataFrame(data), ignore_index=True)

# !IMPORTANT: if AnzahlFall < 0 that row should be ignored. Similar to "AnzahlTodesfall"
data_df = data_df[data_df.AnzahlFall > 0]
data_df.loc[data_df['AnzahlTodesfall'] < 0, 'AnzahlTodesfall'] = 0

df_bd = data_df.drop('Meldedatum', axis='columns').groupby('Bundesland').sum().assign(Datenstand=data_df.loc[0, 'Datenstand'])
df_lk = data_df.drop('Meldedatum', axis='columns').groupby('Landkreis').sum().assign(Datenstand=data_df.loc[0, 'Datenstand'])
del data_df

if os.path.isfile('Data/Bundesland-' + yesterday + '.json'):
    df_bd_yesterday = pd.DataFrame.from_dict(json.load(open('Data/Bundesland-' + yesterday + '.json', 'r', encoding='utf8')), orient='index')
    df_bd.NeuerFall = df_bd.AnzahlFall - df_bd_yesterday.AnzahlFall
    df_bd.NeuerTodesfall = df_bd.AnzahlTodesfall - df_bd_yesterday.AnzahlTodesfall
    # df_bd.loc[df_bd.AnzahlFall < 0, 'AnzahlFall'] = 0
    del df_bd_yesterday

    df_lk_yesterday = pd.DataFrame.from_dict(json.load(open('Data/Landkreis-' + yesterday + '.json', 'r', encoding='utf8')), orient='index')
    df_lk.NeuerFall = df_lk.AnzahlFall - df_lk_yesterday.AnzahlFall
    df_lk.NeuerTodesfall = df_lk.AnzahlTodesfall - df_lk_yesterday.AnzahlTodesfall
    # df_lk.loc[df_lk.AnzahlFall < 0, 'AnzahlFall'] = 0
    del df_lk_yesterday

df_bd.to_json(open('Data/Bundesland-' + today + '.json', 'w', encoding='utf8'), orient='index', force_ascii=False)
df_lk.to_json(open('Data/Landkreis-' + today + '.json', 'w', encoding='utf8'), orient='index', force_ascii=False)
